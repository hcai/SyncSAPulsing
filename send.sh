#!/bin/bash

#############################################################################
#
# Expect script to read out the TTC decoder of ALTI via the menuAltiModule 
# program.
#
# Usage:
#       source decodeTTC.sh ALTI [RATEBER_OF_LINES]
#
#   Please provide the script with the ALTI and the number of
#   lines that you want to read out from the TTC decoder.
#   It will automatically ssh to the appropriate sbc machine.
#
# author:   Tom Kresse (tom.kresse@cern.ch)
# creation date:  13.12.2021
#
############################################################################

EXECUTE='expect /det/lar/project/usersarea/hcai/SyncSAPulsing/lar_ALTI_pulsing.exp '
#EXECUTEEMF='expect /det/lar/usersarea/tkresse/devarea/expect/lar_ALTI_ttc_decoder.exp '
LARSETUP='source /det/lar/project/scripts/env/sod.sh'
ALTIFW2SETUP='source /det/lar/project/scripts/alti/fw2_l1ct_setup.sh'
#ALTIFW2SETUPEMF='source /det/lar/alti/fw2_l1ct_setup.sh'
PGGENERATON='python /det/lar/project/scripts/alti/PG_Generator.py -n 1 -f /tmp/Alti_SAPulsing.dat -p -l'
PGFILE='/tmp/Alti_SAPulsing.dat'

function usage(){
    echo ""
    echo "Script to read the TTC decoder memory of ALTIs"
#    echo ""
#    echo "Usage: source decodeTTC.sh ALTI [RATEBER_OF_LINES]"
#    echo ""
#    echo "for example:"
#    echo "source decodeTTC.sh EMBA 42"
#    echo ""
#    echo "  available ALTIs: EMBA, EMBC, EMECA, EMECC, DTA, DTC, EMF_LEG, EMF_DT"
#    echo ""
}

function sendCmd(){
    SBC=${1}
    SLOT=${2}
    RATE=${RATE}
    echo "The commands to be executed:"
    echo "ssh -o StrictHostKeyChecking=false ${SBC} ''${LARSETUP}' ; '${ALTIFW2SETUP}' ; '${PGGENERATON}' '${RATE}' ; '${EXECUTE}' ${SLOT} ${PGFILE}'"
    ssh -o StrictHostKeyChecking=false ${SBC} ''${LARSETUP}' ; '${ALTIFW2SETUP}' ; '${PGGENERATON}' '${RATE}' ; '${EXECUTE}' '${SLOT}' '${PGFILE}''
}

function EMBA(){
    RATE=${1}
    sendCmd sbc-lar-tcc-emb-11 10 ${RATE}
}

function EMBC(){
    RATE=${1}
    sendCmd sbc-lar-tcc-emb-11 15 ${RATE}
}

function EMECA(){
    RATE=${1}
    sendCmd sbc-lar-tcc-emec-11 4 ${RATE}
}

function EMECC(){
    RATE=${1}
    sendCmd sbc-lar-tcc-emec-11 15 ${RATE}
}

function DTA(){
    RATE=${1}
    sendCmd sbc-lar-tcc-digtrig-11 4 ${RATE}
}

function DTC(){
    RATE=${1}
    sendCmd sbc-lar-tcc-digtrig-11 15 ${RATE}
}

#function EMF_LEG(){
#    RATE=${1}
#    sendCmdEMF sbcemf-inj-01 12 ${RATE}
#}

#function EMF_DT(){
#    RATE=${1}
#    sendCmdEMF sbcemf-tcc-01 12 ${RATE}
#}

function matchALTI(){
    PARTITION=${1}
    RATE=${2}
    if [ "$PARTITION" == "EMBA" ]; then
        EMBA ${RATE}
    elif [ "$PARTITION" == "EMBC" ]; then
        EMBC ${RATE}
    elif [ "$PARTITION" == "EMECA" ]; then
        EMECA ${RATE}
    elif [ "$PARTITION" == "EMECC" ]; then
        EMECC ${RATE}
    elif [ "$PARTITION" == "DTA" ]; then
        DTA ${RATE}
    elif [ "$PARTITION" == "DTC" ]; then
        DTC ${RATE}
#    elif [ "$PARTITION" == "EMF_LEG" ]; then
#        EMF_LEG ${RATE}
#    elif [ "$PARTITION" == "EMF_DT" ]; then
#        EMF_DT ${RATE}
    else
        echo "partition not recognized"
        usage
    fi

}


if [ $# -eq 0 ];  then
    echo "please provide arguments"
    echo ""
    usage
elif [ $# -eq 1 ];  then
    echo "please provide arguments"
    echo ""
    usage
elif [ $# -eq 2 ];  then
    matchALTI ${1} ${2}
else
    echo "Too many options, please only provide  arguments"
    echo ""
    usage
fi
